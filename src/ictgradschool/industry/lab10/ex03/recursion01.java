package ictgradschool.industry.lab10.ex03;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class recursion01 {

    public void start() {
        int result = foo(2);
        System.out.println(result);
    }

    private int foo(int x) {
        if (x <= 1) {
            return 1;
        }
        return x * foo(x - 1);
    }

    public static void main(String[] args) {
        recursion01 s = new recursion01();
        s.start();
    }

}
