package ictgradschool.industry.lab10.ex03;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class recursion02 {

    public void start() {
        double result = bar(3,-2);
        System.out.println(result);
    }

    private double bar(double x, int n) { // x to the power n
        if (n > 1)
            return x * bar(x, n - 1);
        else if (n < 0)
            return 1.0 / bar(x, -n);
        else
            return x;
    }

    public static void main(String[] args) {
        recursion02 s = new recursion02();
        s.start();
    }

}
