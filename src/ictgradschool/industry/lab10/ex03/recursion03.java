package ictgradschool.industry.lab10.ex03;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class recursion03 {

    public void start() {
        bad1();
    }

    private void bad1() {
        System.out.println("This is very good code.");
        bad1();
    }

    public static void main(String[] args) {
        recursion03 s = new recursion03();
        s.start();
    }

}
