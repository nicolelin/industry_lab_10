package ictgradschool.industry.lab10.ex03;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class recursion04 {

    public void start() {
        int bad = bad2(1);
        System.out.println(bad);
    }

    private int bad2(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad2(n - 2);
    }

    public static void main(String[] args) {
        recursion04 s = new recursion04();
        s.start();
    }

}
