package ictgradschool.industry.lab10.ex03;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class recursion05a {

    public void start() {
        int bad = bad3(5);
        System.out.println(bad);
    }

    private int bad3(int n) {
        if (n == 0) {
            return 0;
        }
        return n + bad3(n + 1);
    }

    public static void main(String[] args) {
        recursion05a s = new recursion05a();
        s.start();
    }

}
