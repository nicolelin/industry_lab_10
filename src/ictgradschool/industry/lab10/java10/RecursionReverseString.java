package ictgradschool.industry.lab10.java10;

/**
 * Created by ylin183 on 4/04/2017.
 */
public class RecursionReverseString {


    public static void main(String[] args) {
        RecursionReverseString s = new RecursionReverseString();
//        s.reverse("Cat");
        System.out.print(s.reverse("CatDog"));
    }

    String reversedString = "";

    public String reverse(String s) {
        if (s.length() <= 1) { // BASE CASE
            return s;
        }
        return reverse(s.substring(1))+s.charAt(0); // RECURSION
    }

//    public String reverse(String s) {
//        if (s.length()==1) {
//            return "" + s.charAt(0);
//        }
//        return reverse("" + s.charAt(s.length()-1));
//    }
}
